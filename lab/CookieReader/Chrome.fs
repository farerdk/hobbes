module Chrome 
    open System
    open System.Security.Cryptography

    

    let private cookieNames = 
        [
            """FedAuth"""
            """FedAuth1"""
            """HostAuthentication"""
            """VstsSession"""
        ]
        
    let whereNames =
        System.String.Join(" OR ", cookieNames
                                   |> List.map(sprintf "name = '%s'"))


    let unprotect encryptedData = 
        ProtectedData.Unprotect(encryptedData, null, DataProtectionScope.CurrentUser)
        |> Text.Encoding.ASCII.GetString

    let read dbPath = 
        let connectionString = 
            if IO.File.Exists(dbPath) |> not then 
                System.IO.FileNotFoundException("Cant find cookie store",dbPath) |> raise
            "Data Source=" + dbPath + ";pooling=false";
        use conn = new System.Data.SQLite.SQLiteConnection(connectionString)
        use cmd = conn.CreateCommand()
        cmd.CommandText <- sprintf "SELECT name, encrypted_value, host_key FROM cookies WHERE host_key = '.dev.azure.com' AND (%s)" whereNames
        try
            conn.Open()
            use reader = cmd.ExecuteReader()
            [while (reader.Read()) do
                let plainText =
                    reader.[1] :?> byte []
                    |> unprotect

                yield reader.GetString(0), plainText, reader.GetString(2)]
        finally
            conn.Close()
