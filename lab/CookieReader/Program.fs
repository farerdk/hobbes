﻿// Learn more about F# at http://fsharp.org
// See the 'F# Tutorial' project for more help.
open Arguments
open Argu

[<EntryPoint>]
let main argv =
    let parser = ArgumentParser.Create<CommandLineArguments>(programName = "CookieReader.exe")
    let arguments = parser.Parse argv
    let path = 
        match arguments.TryGetResult Path with
        None -> 
            System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData) 
              + @"\Google\Chrome\User Data\Default\Cookies"
        | Some p -> p
    let reader = 
        match arguments.TryGetResult Browser with
        None
        | Some Browsers.Chrome ->
            Chrome.read 
    let outFile =     
        match arguments.TryGetResult Output_File with
        Some file -> file
        | None -> //"../../../../metrics-core/metrics-core/cookies.json"
            "cookies.json"
    let cookies = 
        """[""" + System.String.Join(",", 
                path 
                |> reader
                |> List.map(fun (n,v,_) -> sprintf "[\"%s\",\"%s\"]" n v)) + "]"
    System.IO.File.WriteAllText(outFile, cookies)
    0 // return an integer exit code
