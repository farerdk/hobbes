module Arguments
    open Argu
    type Browsers = 
        Chrome
    type CommandLineArguments =
        | Browser of Browsers
        | Path of string
        | Output_File of string
        with
            interface IArgParserTemplate with
                member x.Usage =
                    match x with
                    Browser _ -> "Specify the browser used to login to Azure DevOps services"
                    | Path _ -> "The path to where the cookies are stored"
                    | Output_File _ -> "The file the cookies will be written to"